

#include "gridOverlayGPU.h"

 __device__ double collinearValue(const double p1x, const double p1y,
                            const double p2x, const double p2y,
                            const double p3x, const double p3y)
{
    return ((p3y - p1y) * (p2x - p1x)) - ((p2y - p1y) * (p3x - p1x));
}

 __device__ unsigned int cellHashfunction( const int x, const int y, const int S )
{
    // uses this hash recipe:
    // h(k)  = (f(a, k)+b) %p % S
    // S is size of the table,
    // a and b are positive constants less than p
    // p is a large prime number larger than any number that will be hashed
    // f(a,k) is an interaction of a and k, in this case f(a,k) = a*k
    // x and y are expected to not be too huge,  so we will use
    // p = 2,147,483,647 (largest prime signed 32 bit integer)
    // a = 13
    // b = 654,681
    
    // we hash both integers,  then combine their hashes
    const int p = 2147483647;
    const int a = 13;
    const int b = 654681;
    return ((((a*x) + b) % p ) % s) ^ ( ((((a*y) + b) % p ) % s)<<5 ) 
}

__global__ void createCountHash( const double * x1, const double * y1, 
                                const double * x2, const double * y2,
                                const int dataSize, 
                                const double otherBboxX1, const double otherBboxX2,
                                int * hashTableCount, const int hashTableCountSize,
                                const double cellSize )
                                
{
    // each thread finds the cells that a segment falls into,  then increments
    // the counts in those cells.  the counts are maintained in the hash table.
    
    unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
   
    // if the seg is outside the bbox of the other region, just skip it.
    if(x2[id] < otherBboxX1 || x1[id] > otherBboxX2 )
        continue;

    int x,y, startGridX, startGridY, endGridX, endGridY;
    bool goingUp, addLeft, addDown, firstLoopIteration;
    int currCellX, currCellY;
    double diagX, diagHiY, diagLoY, colinValHi, colinValLo;
    unsigned int hashVal;

    // determine the cell in which this segment begins
    // We **ALWAYS** assume the left seg endpoint is less than
    // the right!
    startGridX = int(x1[id] / cellSize );
    startGridY = int(y1[id] / cellSize );
    endGridX = int(x2[id] / cellSize );
    endGridY = int(y2[id] / cellSize );

    // indicate if the slope is positive (i.e.,  the line 
    // goes up when traveling from less point to greater point)
    goingUp = true;
    if( y2[id]-y1[id] < 0 )
        goingUp = false;
    
    // if we end on a vertical cell boundary,  
    // put it in the left cell.
    if( fmod( x2[id], cellSize) == 0.0){
        endGridX -= 1;
    }

    // if we are going up or are horizontal 
    // and end on a horizontal boundary
    // only put the end point in the lower grid
    if( goingUp && fmod( y2[id], cellSize) == 0.0){
        endGridY -= 1;
    }

    currCellX = startGridX;
    currCellY = startGridY;
    

    // now we have everythin set up
    // next, we will walk down the segment,  adding
    // all grid cells it intersects

    // The first cell is a special case.  Handle it before 
    // the main loop
    // we need to add its left and below cells

    // ALSO need to check vert or horiz seg on cell boundary!!
    //  Horizontal and vertical are special cases,  handle them seperately

    addLeft = fmod( x1[id], cellSize) == 0.0 ;
    addDown = fmod( y1[id], cellSize) == 0.0 ;
    
    //SPECIAL CASE.  Horizontal and vertical segs
    if( y1[id] == y2[id] ) {  // horizontal
        if( addLeft && addDown ){
            currCellX--;
            currCellY--;
        }
        else if( addLeft ){
            currCellX--;
        }
        else if( addDown ){ // on a cell boundary, put in left cell
            currCellY--;
        }
        for( int j = currCellX; j <= endGridX; j++ )
        {
            // add the segment to the hash table (count it in this case)
            hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
            atomicAdd( &hashTableCount[ hashVal ], 1);
            // record cell:  usedCells.insert( currCell );
            currCellX++;
        }
        // we are finished adding a horizontal segment
        return;
    }

    if( x1[id] == x2[id] ) {  // vertical
        if( addLeft && addDown ){
            currCellX--;
            currCellY--;
        }
        else if( addLeft ){
            currCellX--;
        }
        else if( addDown ){ // on a cell boundary, put in left cell
            currCellY--;
        }
        for( int j = currCellY; j <= endGridY; j++ )
        {
            // add the segment to the hash table (count it in this case)
            hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
            atomicAdd( &hashTableCount[ hashVal ], 1);
            // record cell:  usedCells.insert( currCell );
            // we are always going up in this case
            currCellY++;
        }
        return;
    }

    // handle the first cell as a special case
    // check if we need to add a diag, left, or below

    // boundary check for the diagonal cell
    if( addLeft && addDown )
    {
        currCellX--;
        currCellY--;
        hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
        atomicAdd( &hashTableCount[ hashVal ], 1);
        //std::cerr << "L&D: " << currCell.x << "," << currCell.y << std::endl; 
        currCellX++;
        // if the seg is going down,  we want to start at the reduced Y
        // if not, we want to start on the orig Y cell
        if( goingUp )
        {
            currCellY++;
            //std::cerr << "up" << std::endl;
        }
    }
    // if left end point is on a cell boundary, add the neighbor cell
    else {
        if( addLeft )
        {
            currCellX--;
            hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
            atomicAdd( &hashTableCount[ hashVal ], 1);
            //std::cerr << "L: " << currCell.x << "," << currCell.y << std::endl; 
            currCellX++;
        }
        // repeat the boundary check for the cell below currCell 
        else if( addDown )
        {
            currCellY--;
            //std::cerr << "add down but no add"<<std::endl;
            // only put Y seg back if seg is going upwards
            if( goingUp ){
                // since we record the current cell in the loop,
                // only record the cell if we are going up
                // otherwise,  it will get recorded twice.
                hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
                atomicAdd( &hashTableCount[ hashVal ], 1);
                //std::cerr << "D: " << currCell.x << "," << currCell.y << std::endl; 
                currCellY++;
            }
        }
    }

    //  if we are going down,  and the final point is on a 
    // horizontal cell boundary,  add the cell below.
    // even if it is at the diagonal,  we only need to add the cell below,
    // since we add the lower left diag cell on diags.
    if( !goingUp 
            &&  fmod( y2[i], cellSize) == 0.0){
        int lastExtraCellX = endGridX;
        int lastExtraCellY = endGridY-1;
        hashVal = cellHashfunction( lastExtraCellX, lastExtraCellY, hashTableCountSize );
        atomicAdd( &hashTableCount[ hashVal ], 1);
        //std::cerr << "last extra cell: " << lastExtraCell.x << ", "<< lastExtraCell.y << std::endl;
    }

    while( true )
    {
        // record the current cell
        hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
        atomicAdd( &hashTableCount[ hashVal ], 1);
        //std::cerr << "add: " << currCell.x << "," << currCell.y << std::endl;
        // check to see if we are finished (if the curr cell 
        // is equal to the end cell)
        if( currCellX >= endGridX && 
                ((goingUp && currCellY >= endGridY) 
                 || ( !goingUp && currCellY <= endGridY )
                )
          )
            break;

        // Now we need to find which cell to visit next.  
        // find the points for uppr right and lower right corner
        // of the current cell. use colinearity test to see
        // if this segment travels up, diagonal-up, right,
        // diagnal-down,  or down.
        diagX = (currCellX+1) * cellSize;
        diagHiY = (currCellY+1) * cellSize;
        diagLoY = (currCellY) * cellSize;
        colinValHi = collinearValue( x1[i], y1[i], diagX, diagHiY, x2[i], y2[i] ); 
        colinValLo = collinearValue( x1[i], y1[i], diagX, diagLoY, x2[i], y2[i] );
        if( abs( colinValHi ) < 0.0000001 )
        {
            // goes through upperdiagonal
            // increas currCell x and y.
            // add the diagonal cell.  
            // We only add adjacents for left and lower boundary
            currCellX++;
            currCellY++;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            //std::cerr << "diag Hi" << std::endl;
        }
        else if( colinValHi > 0 )
        {
            // goes through upper cell boundary
            currCellY++;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            // cell is set for nect iteration
            //std::cerr << "Hi" << std::endl;
        }
        else if( abs(colinValLo) < 0.0000001 )
        {
            // goes through lower diagonal.  
            // add the cell below and the cell diagonal
            currCellY--;
            hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
            atomicAdd( &hashTableCount[ hashVal ], 1);

            currCellX++;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            //std::cerr << "diag Lo" << std::endl;
        }
        else if( colinValLo > 0 )
        {
            // goes throug right cell boundary
            // add the cell to the right
            currCellX++;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            //std::cerr << "Right" << std::endl;
        } else
        {
            // it goes though the lower cell boundary
            currCellY--;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            //std::cerr << "Lo" << std::endl;
        }
    }
}

__global__ void createHash( const double * x1, const double * y1, 
                                const double * x2, const double * y2,
                                const int dataSize, 
                                const double otherBboxX1, const double otherBboxX2,
                                int * hashTableCount, const int hashTableCountSize,
                                int * hashTable, const int hashTableSize,
                                int * cellXList, int * cellYList,
                                const double cellSize )
                                
{

    // each thread finds the cells that a segment falls into,  then adds
    // the segment index to the hash table for that cell.
    
    unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
   
    // if the seg is outside the bbox of the other region, just skip it.
    if(x2[id] < otherBboxX1 || x1[id] > otherBboxX2 )
        continue;

    int x,y, startGridX, startGridY, endGridX, endGridY;
    bool goingUp, addLeft, addDown, firstLoopIteration;
    int currCellX, currCellY;
    double diagX, diagHiY, diagLoY, colinValHi, colinValLo;
    unsigned int hashVal;

    // determine the cell in which this segment begins
    // We **ALWAYS** assume the left seg endpoint is less than
    // the right!
    startGridX = int(x1[id] / cellSize );
    startGridY = int(y1[id] / cellSize );
    endGridX = int(x2[id] / cellSize );
    endGridY = int(y2[id] / cellSize );

    // indicate if the slope is positive (i.e.,  the line 
    // goes up when traveling from less point to greater point)
    goingUp = true;
    if( y2[id]-y1[id] < 0 )
        goingUp = false;
    
    // if we end on a vertical cell boundary,  
    // put it in the left cell.
    if( fmod( x2[id], cellSize) == 0.0){
        endGridX -= 1;
    }

    // if we are going up or are horizontal 
    // and end on a horizontal boundary
    // only put the end point in the lower grid
    if( goingUp && fmod( y2[id], cellSize) == 0.0){
        endGridY -= 1;
    }

    currCellX = startGridX;
    currCellY = startGridY;
    

    // now we have everythin set up
    // next, we will walk down the segment,  adding
    // all grid cells it intersects

    // The first cell is a special case.  Handle it before 
    // the main loop
    // we need to add its left and below cells

    // ALSO need to check vert or horiz seg on cell boundary!!
    //  Horizontal and vertical are special cases,  handle them seperately

    addLeft = fmod( x1[id], cellSize) == 0.0 ;
    addDown = fmod( y1[id], cellSize) == 0.0 ;
    
    //SPECIAL CASE.  Horizontal and vertical segs
    if( y1[id] == y2[id] ) {  // horizontal
        if( addLeft && addDown ){
            currCellX--;
            currCellY--;
        }
        else if( addLeft ){
            currCellX--;
        }
        else if( addDown ){ // on a cell boundary, put in left cell
            currCellY--;
        }
        for( int j = currCellX; j <= endGridX; j++ )
        {
            // add the segment to the hash table (count it in this case)
            hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
            hashVal += atomicAdd( &hashTableCount[ hashVal ], -1);
            hashTable[ hashVal ] = id;
            cellXList[ hashVal ] = currCellX;
            cellYList[ hashVal ] = currCellY;
            currCellX++;
        }
        // we are finished adding a horizontal segment
        return;
    }

    if( x1[id] == x2[id] ) {  // vertical
        if( addLeft && addDown ){
            currCellX--;
            currCellY--;
        }
        else if( addLeft ){
            currCellX--;
        }
        else if( addDown ){ // on a cell boundary, put in left cell
            currCellY--;
        }
        for( int j = currCellY; j <= endGridY; j++ )
        {
            // add the segment to the hash table (count it in this case)
            hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
            hashVal += atomicAdd( &hashTableCount[ hashVal ], -1);
            hashTable[ hashVal ] = id;
            cellXList[ hashVal ] = currCellX;
            cellYList[ hashVal ] = currCellY;
            // we are always going up in this case
            currCellY++;
        }
        return;
    }

    // handle the first cell as a special case
    // check if we need to add a diag, left, or below

    // boundary check for the diagonal cell
    if( addLeft && addDown )
    {
        currCellX--;
        currCellY--;
        hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
        hashVal += atomicAdd( &hashTableCount[ hashVal ], -1);
        hashTable[ hashVal ] = id;
        cellXList[ hashVal ] = currCellX;
        cellYList[ hashVal ] = currCellY;
        //std::cerr << "L&D: " << currCell.x << "," << currCell.y << std::endl; 
        currCellX++;
        // if the seg is going down,  we want to start at the reduced Y
        // if not, we want to start on the orig Y cell
        if( goingUp )
        {
            currCellY++;
            //std::cerr << "up" << std::endl;
        }
    }
    // if left end point is on a cell boundary, add the neighbor cell
    else {
        if( addLeft )
        {
            currCellX--;
            hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
            hashVal += atomicAdd( &hashTableCount[ hashVal ], -1);
            hashTable[ hashVal ] = id;
            cellXList[ hashVal ] = currCellX;
            cellYList[ hashVal ] = currCellY;
            //std::cerr << "L: " << currCell.x << "," << currCell.y << std::endl; 
            currCellX++;
        }
        // repeat the boundary check for the cell below currCell 
        else if( addDown )
        {
            currCellY--;
            //std::cerr << "add down but no add"<<std::endl;
            // only put Y seg back if seg is going upwards
            if( goingUp ){
                // since we record the current cell in the loop,
                // only record the cell if we are going up
                // otherwise,  it will get recorded twice.
                hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
                hashVal += atomicAdd( &hashTableCount[ hashVal ], -1);
                hashTable[ hashVal ] = id;
                cellXList[ hashVal ] = currCellX;
                cellYList[ hashVal ] = currCellY;
                //std::cerr << "D: " << currCell.x << "," << currCell.y << std::endl; 
                currCellY++;
            }
        }
    }

    //  if we are going down,  and the final point is on a 
    // horizontal cell boundary,  add the cell below.
    // even if it is at the diagonal,  we only need to add the cell below,
    // since we add the lower left diag cell on diags.
    if( !goingUp 
            &&  fmod( y2[i], cellSize) == 0.0){
        int lastExtraCellX = endGridX;
        int lastExtraCellY = endGridY-1;
        hashVal = cellHashfunction( lastExtraCellX, lastExtraCellY, hashTableCountSize );
        hashVal += atomicAdd( &hashTableCount[ hashVal ], -1);
        hashTable[ hashVal ] = id;
        cellXList[ hashVal ] = currCellX;
        cellYList[ hashVal ] = currCellY;
        //std::cerr << "last extra cell: " << lastExtraCell.x << ", "<< lastExtraCell.y << std::endl;
    }

    while( true )
    {
        // record the current cell
        hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
        hashVal += atomicAdd( &hashTableCount[ hashVal ], -1);
        hashTable[ hashVal ] = id;
        cellXList[ hashVal ] = currCellX;
        cellYList[ hashVal ] = currCellY;
        //std::cerr << "add: " << currCell.x << "," << currCell.y << std::endl;
        // check to see if we are finished (if the curr cell 
        // is equal to the end cell)
        if( currCellX >= endGridX && 
                ((goingUp && currCellY >= endGridY) 
                 || ( !goingUp && currCellY <= endGridY )
                )
          )
            break;

        // Now we need to find which cell to visit next.  
        // find the points for uppr right and lower right corner
        // of the current cell. use colinearity test to see
        // if this segment travels up, diagonal-up, right,
        // diagnal-down,  or down.
        diagX = (currCellX+1) * cellSize;
        diagHiY = (currCellY+1) * cellSize;
        diagLoY = (currCellY) * cellSize;
        colinValHi = collinearValue( x1[i], y1[i], diagX, diagHiY, x2[i], y2[i] ); 
        colinValLo = collinearValue( x1[i], y1[i], diagX, diagLoY, x2[i], y2[i] );
        if( abs( colinValHi ) < 0.0000001 )
        {
            // goes through upperdiagonal
            // increas currCell x and y.
            // add the diagonal cell.  
            // We only add adjacents for left and lower boundary
            currCellX++;
            currCellY++;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            //std::cerr << "diag Hi" << std::endl;
        }
        else if( colinValHi > 0 )
        {
            // goes through upper cell boundary
            currCellY++;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            // cell is set for nect iteration
            //std::cerr << "Hi" << std::endl;
        }
        else if( abs(colinValLo) < 0.0000001 )
        {
            // goes through lower diagonal.  
            // add the cell below and the cell diagonal
            currCellY--;
            hashVal = cellHashfunction( currCellX, currCellY, hashTableCountSize );
            hashVal += atomicAdd( &hashTableCount[ hashVal ], -1);
            hashTable[ hashVal ] = id;
            cellXList[ hashVal ] = currCellX;
            cellYList[ hashVal ] = currCellY;

            currCellX++;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            //std::cerr << "diag Lo" << std::endl;
        }
        else if( colinValLo > 0 )
        {
            // goes throug right cell boundary
            // add the cell to the right
            currCellX++;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            //std::cerr << "Right" << std::endl;
        } else
        {
            // it goes though the lower cell boundary
            currCellY--;
            //cellDict[ currCell ].push_back( i );
            //usedCells.insert( currCell );
            //std::cerr << "Lo" << std::endl;
        }
    }
}


void gridOverlayGPU( const thrust::host_vector<double> & v1x1,  const thrust::host_vector<double> & v1y1, 
                    const thrust::host_vector<double> & v1x2,  const thrust::host_vector<double> & v1y2, 
                    const thrust::host_vector<int> & v1la, const thrust::host_vector<int> & v1lb, 
                    const seg2D & v1Bbox,
                    
                    const thrust::host_vector<double> & v2x1, const thrust::host_vector<double> & v2y1, 
                    const thrust::host_vector<double> & v2x2, const thrust::host_vector<double> & v2y2, 
                    const thrust::host_vector<int> & v2la, const thrust::host_vector<int> & v2lb, 
                    const seg2D &v2Bbox,
                    
                    thrust::host_vector<double> & r1x1, thrust::host_vector<double> & r1y1, 
                    thrust::host_vector<double> & r1x2, thrust::host_vector<double> & r1y2, 
                    thrust::host_vector<int> & r1la, thrust::host_vector<int> & r1lb, 
                    thrust::host_vector<int> & r1ola, thrust::host_vector<int> & r1olb,

                    thrust::host_vector<double> & r2x1, thrust::host_vector<double> & r2y1, 
                    thrust::host_vector<double> & r2x2, thrust::host_vector<double> & r2y2, 
                    thrust::host_vector<int> & r2la, thrust::host_vector<int> & r2lb, 
                    thrust::host_vector<int> & r2ola, thrust::host_vector<int> & r2olb, 
                    const double segsPerCell)
{
    
    // Step 1: allocate device memory
    thrust::device_vector< double > dv1x1( v1x1 ), dv1y1( v1y1 ), dv1x2( v1x2 ), dv1y2( v1y2 ),
                                    dv2x1( v2x1 ), dv2y1( v2y1 ), dv2x2( v2x2 ), dv2y2( v2y2 ), 
    thrust::device_vector< int > dv1la( v1la ), dv1lb( v1lb ),              
                                 dv2la( v2la ), dv2lb( v2lb );

    // Calculate the size of a cell
    const double SEGSAMPLERATE = .1;
    int loopStep = v1x1.size() / (1+int((SEGSAMPLERATE * v1x1.size()))); 
    double sum = 0;
    for( int i = 0; i < r1.size(); i+= loopStep )
    {
        sum +=((v1x2[i]-v1x1[i])*(v1x2[i]-v1x1[i]))+((v1y2[i]-v1y1[i])*(v1y2[i]-v1y1[i])); 
    }

    loopStep = r2.size() / (1+int((SEGSAMPLERATE * r2.size()))); 
    for( int i = 0; i < r2.size(); i+= loopStep )
    {
        sum +=((v2x2[i]-v2x1[i])*(v2x2[i]-v2x1[i]))+((v2y2[i]-v2y1[i])*(v2y2[i]-v2y1[i])); 
    }

    double avgLen = std::sqrt( sum/(v1x1.size()+v2x1.size()) );
    double cellSize = avgLen/segsPerCell;
    //cellSize = 1; 
    std::cerr << "loopStep: " << loopStep << std::endl
         << "sum:      " << sum << std::endl
         << "avgLen:   " << avgLen << std::endl
         << "cellSize: " << cellSize << std::endl;


    // Step 2:  Create a count hash table to find the amount needed hash table size.
    // for hash table size,  we will use a hash table 1.3 times the size of the 
    // number of segments
    thrust::device_vector< int > dv1HashTableCount( v1x1.size() * 1.3, 0 );
    thrust::device_vector< int > dv2HashTableCount( v2x1.size() * 1.3, 0 );

    // we will use a 1 dimensional grid
    dim3 TPBhash( 1024,0,0 );
    dim3 BPGv1hash(( v1x1.size()+ TPBhash.x-1 )/ TBPhash.x, 0, 0 ); 
    dim3 BPGv2hash(( v2x1.size()+ TPBhash.x-1 )/ TBPhash.x, 0, 0 ); 
    
    createCountHash <<<BPGv1hash, TPBhash>>> 
        ( thrust::raw_pointer_cast( dv1x1 ), thrust::raw_pointer_cast( dv1y1), 
            thrust::raw_pointer_cast( dv1x2 ), thrust::raw_pointer_cast( dv1y2 ),
            dv1x1.size(), 
            v2Bbox.x1, v2Bbox.x2,
            thrust::raw_pointer_cast( dv1HashTableCount ), dhashTableCount.size(),
            cellSize );

    createCountHash <<<BPGv21hash, TPBhash>>> 
        ( thrust::raw_pointer_cast( dv2x1 ), thrust::raw_pointer_cast( dv2y1), 
            thrust::raw_pointer_cast( dv2x2 ), thrust::raw_pointer_cast( dv2y2 ),
            dv2x1.size(), 
            v1Bbox.x1, v1Bbox.x2,
            thrust::raw_pointer_cast( dv2HashTableCount ), dhashTableCount.size(),
            cellSize );

    // Step 3:  We now know how many segs map to each hash location.
    // compute an inclusive prefix sum se we can reserve the appropriate space

    thrust::inclusive_scan( thrust::raw_pointer_cast( dv1HashTableCount ),thrust::raw_pointer_cast( dv1HashTableCount ) + dv1HashTableCount.size() );
    thrust::inclusive_scan( thrust::raw_pointer_cast( dv2HashTableCount ),thrust::raw_pointer_cast( dv2HashTableCount ) + dv2HashTableCount.size() );

    // Step 4:  We can now allocate the exact size of the needed hash table,  
    // and use the prefix summed counts to identify exactly where a segment will be stored in the hash table!

    int hv1HashTableSize = dv1HashTableCount[ dv1HashTableCount.size()-1 ];
    int hv2HashTableSize = dv2HashTableCount[ dv2HashTableCount.size()-1 ];
    thrust::device_vector< int > dv1HashTable( hv1HashTableSize ), dv1CellXList( hv2HashTableSize ), dv1CellXList( hv2HashTableSize );
    thrust::device_vector< int > dv2HashTable( hv2HashTableSize ), dv2CellXList( hv2HashTableSize ), dv2CellXList( hv2HashTableSize );

    // Step 5: We can now assign items to the hash table.
    // We will also record the cells used as keys in the hash table.
    createHash <<<BPGv1hash, TPBhash>>> 
        ( thrust::raw_pointer_cast( dv1x1 ), thrust::raw_pointer_cast( dv1y1), 
            thrust::raw_pointer_cast( dv1x2 ), thrust::raw_pointer_cast( dv1y2 ),
            dv1x1.size(), 
            v2Bbox.x1, v2Bbox.x2,
            thrust::raw_pointer_cast( dv1HashTableCount ), dhashTableCount.size(),
            thrust::raw_pointer_cast( dv1HashTable ), dv1HashTable.size(),
            thrust::raw_pointer_cast( dv1CellXList ), thrust::raw_pointer_cast( dv1CellYList ),
            cellSize );

    createHash <<<BPGv21hash, TPBhash>>> 
        ( thrust::raw_pointer_cast( dv2x1 ), thrust::raw_pointer_cast( dv2y1), 
            thrust::raw_pointer_cast( dv2x2 ), thrust::raw_pointer_cast( dv2y2 ),
            dv2x1.size(), 
            v1Bbox.x1, v1Bbox.x2,
            thrust::raw_pointer_cast( dv2HashTableCount ), dhashTableCount.size(),
            thrust::raw_pointer_cast( dv2HashTable ), dv2HashTable.size(),
            thrust::raw_pointer_cast( dv2CellXList ), thrust::raw_pointer_cast( dv2CellYList ),
            cellSize );
    
    // Step 6:  We know have the hash tables and the cells used.  Sort the cells,
    // unique the cells, then set intersect the cells.
   HAERAERE 

}

