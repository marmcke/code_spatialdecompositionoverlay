
#include <limits>
#include <vector>
#include <iostream> 

#ifndef GRIDOVERLAY_H
#define GRIDOVERLAY_H 

struct indexedPoint {
    unsigned int index;
    double x,y;
    indexedPoint(): index(0), x(0.0), y(0.0)
    {   }
    indexedPoint( int index, double x, double y): index( index ), x( x ), y( y )
    {   }
    bool operator == ( const indexedPoint & rhs ) const
    {
        return index == rhs.index && x == rhs.x && y == rhs.y;
    }
};

struct  {
    bool operator () ( 
            const indexedPoint & lhs,
            const indexedPoint & rhs ) const
    {
        return lhs.index < rhs.index 
                || (lhs.index == rhs.index && lhs.x < rhs.x )
                || (lhs.index == rhs.index && lhs.x == rhs.x && lhs.y < rhs.y) ; 
    }
}indexedPointLess;


struct seg2D
{
    double x1,y1,x2,y2;
    int interiorAbove, interiorBelow;
    int overlapInteriorAbove, overlapInteriorBelow;
    seg2D( ): x1(std::numeric_limits<double>::max()),
    y1(std::numeric_limits<double>::max()),
    x2(-std::numeric_limits<double>::max()),
    y2(-std::numeric_limits<double>::max()),
    interiorAbove( 0 ), interiorBelow( 0 ),
    overlapInteriorAbove( 0 ),
    overlapInteriorBelow( 0 )
    {   }

    seg2D( double x1, double y1, double x2, double y2,
            int interiorAbove, int interiorBelow, 
            int overlapInteriorAbove, int overlapInteriorBelow ): 
        x1(x1), y1(y1), x2(x2), y2(y2),
        interiorAbove( interiorAbove ), interiorBelow( interiorBelow ),
        overlapInteriorAbove( overlapInteriorAbove ),
        overlapInteriorBelow( overlapInteriorBelow )
    {   
        if( x2 < x1 
            || (x2 == x1 && y2 < y1 ) )
        {
            double tmp = x1;
            x1 = x2;
            x2 = tmp;
            tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
    }
    double inline squaredLength() const
    {
        return ((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1));
    }
    void print( bool newline=true ) const
    {
        std::cerr << "(" << x1 << ","<< y1<<")("<<x2<<","<<y2<<") "
            << interiorAbove << ":"<< interiorBelow 
            << " - " << overlapInteriorAbove << ":" << overlapInteriorBelow;
        if( newline ) 
            std::cerr << std::endl;
    }
    void orderEndPoints()
    {
        if( x2 < x1 
            || (x2 == x1 && y2 < y1 ) )
        {
            double tmp = x1;
            x1 = x2;
            x2 = tmp;
            tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
    }
    /**
     *  if we are using this seg to store a bounding box,  this function will
     *  grow the bounding box based on the segment passed to it
     *
     *  for bbox,  x1,y1 is lower left, x2 y2 is upper right
     *  
     */
    void growBbox( const seg2D s )
    {
        if( s.x1 <= x1 ) x1 = s.x1;
        if( s.x2 <= x1 ) x1 = s.x2;
        if( s.x1 >= x2 ) x2 = s.x1;
        if( s.x2 >= x2 ) x2 = s.x2;

        if( s.y1 <= y1 ) y1 = s.y1;
        if( s.y2 <= y1 ) y1 = s.y2;
        if( s.y1 >= y2 ) y2 = s.y1;
        if( s.y2 >= y2 ) y2 = s.y2;
    }

    /**
     * test if two bounding boxes intersect.  Again, we are using segs to 
     * store bouding boxes.
     *
     * for bbox,  x1,y1 is lower left, x2 y2 is upper right 
     */
    bool bBoxIntersect( const seg2D s ) const
    {
        // NEED TO IMPLEMENT
        return true;
    }
};

void gridOverlayGPU( const thrust::host_vector<double> & v1x1,  const thrust::host_vector<double> & v1y1, 
                    const thrust::host_vector<double> & v1x2,  const thrust::host_vector<double> & v1y2, 
                    const thrust::host_vector<int> & v1la, const thrust::host_vector<int> & v1lb, 
                    const seg2D & v1Bbox,
                    
                    const thrust::host_vector<double> & v2x1, const thrust::host_vector<double> & v2y1, 
                    const thrust::host_vector<double> & v2x2, const thrust::host_vector<double> & v2y2, 
                    const thrust::host_vector<int> & v2la, const thrust::host_vector<int> & v2lb, 
                    const seg2D &v2Bbox,
                    
                    thrust::host_vector<double> & r1x1, thrust::host_vector<double> & r1y1, 
                    thrust::host_vector<double> & r1x2, thrust::host_vector<double> & r1y2, 
                    thrust::host_vector<int> & r1la, thrust::host_vector<int> & r1lb, 
                    thrust::host_vector<int> & r1ola, thrust::host_vector<int> & r1olb,

                    thrust::host_vector<double> & r2x1, thrust::host_vector<double> & r2y1, 
                    thrust::host_vector<double> & r2x2, thrust::host_vector<double> & r2y2, 
                    thrust::host_vector<int> & r2la, thrust::host_vector<int> & r2lb, 
                    thrust::host_vector<int> & r2ola, thrust::host_vector<int> & r2olb, 
                    const double segsPerCell = .2 );

#endif

