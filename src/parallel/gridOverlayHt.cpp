/*
 * The MIT License (MIT)
 * Copyright (c) <2017> <Mark McKenney, Yanan Da>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */

#include <algorithm>
#include "gridOverlay.h"
#include "pAppendVector.h"
#include "stopwatch.h"
#include <set>
#include <unordered_map>
#include <vector>
#include <cmath>
#include <iostream>
#include <omp.h> 

//#define DEBUGPRINT

/**
 When traveling from point p1 to pont p2, 
 do we take a left or right turn to travel on th point p3?

 uses the signed area of the triangle imposed on the plane
 by the points for the calculation.

 **input**: 3 points, p1, p2, p3

 **output**: if it is a left turn,  the result is positive,
            if it is a right turn,  the result is negative,
            if there is no turn, the result is 0
*/
double inline collinearValue(const double p1x, const double p1y,
                            const double p2x, const double p2y,
                            const double p3x, const double p3y)
{
    return ((p3y - p1y) * (p2x - p1x)) - ((p2y - p1y) * (p3x - p1x));
}



struct cell
{
    int x,y;
};

struct cellHash
{
    std::size_t operator()(const cell & k) const
    {
        return (std::hash<int>()(k.x))
            ^ (std::hash<int>()(k.y) << 9);
    }
};

struct cellEqual {
    bool operator()(const cell & lhs, const cell & rhs) const
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }
};

struct cellLess {
    bool operator()(const cell & lhs, const cell & rhs) const
    {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y <rhs.y );
    }
};



void constructSegs( 
        const std::vector<seg2D> & r1,
        pAppendVector & pois, 
        std::vector<seg2D> & newR1 );

void redBlueLineSegmentIntersection(
        const std::vector<seg2D>& r1, std::unordered_map< cell, std::vector<int>, cellHash, cellEqual> & cellDict1, 
        const std::vector<seg2D> &r2, std::unordered_map< cell, std::vector<int>, cellHash, cellEqual> & cellDict2,
        const std::vector< cell > & usedCells,
        pAppendVector & r1Points, pAppendVector & r2Points );    



void computeTopology( 
        std::vector<seg2D> &newR1,
        const std::vector<seg2D> &r2, 
        const std::set< cell, cellLess > & usedCells2,
        std::unordered_map< cell, std::vector<int>, cellHash, cellEqual> & cellDict2,
        const int cellSize );


void assignCells( const std::vector<seg2D> & r, const double cellSize,
                    std::unordered_map< cell, std::vector<int>, cellHash, cellEqual > &cellDict,
                    std::set< cell, cellLess > & usedCells,
                    const seg2D& otherBbox )
{
    int x,y, startGridX, startGridY, endGridX, endGridY;
    bool goingUp, addLeft, addDown, firstLoopIteration;
    cell currCell;
    double diagX, diagHiY, diagLoY, colinValHi, colinValLo;
    //std::cerr << "===== assign cells =======+++++++"<<std::endl;
    for( int i = 0; i< r.size(); i++ )
    {

        //std::cerr << "===== currSeg: "; r[i].print( );

        // if the seg is completely out of the bbox on the
        // right or left,  just skip it
        if(r[i].x2 < otherBbox.x1 || r[i].x1 > otherBbox.x2 )
            continue;

        // determine the cell in which this segment begins
        // We **ALWAYS** assume the left seg endpoint is less than
        // the right!
        startGridX = int(r[i].x1 / cellSize );
        startGridY = int(r[i].y1 / cellSize );
        endGridX = int(r[i].x2 / cellSize );
        endGridY = int(r[i].y2 / cellSize );
       
        // indicate if the slope is positive (i.e.,  the line 
        // goes up when traveling from less point to greater point)
        goingUp = true;
        if( r[i].y2-r[i].y1 < 0 )
            goingUp = false;

        // if we end on a vertical cell boundary,  
        // put it in the left cell.
        if( std::fmod( r[i].x2, cellSize) == 0.0){
            endGridX -= 1;
        }

        // if we are going up or are horizontal 
        // and end on a horizontal boundary
        // only put the end point in the lower grid
        if( goingUp && std::fmod( r[i].y2, cellSize) == 0.0){
            endGridY -= 1;
        }
       
        currCell.x = startGridX;
        currCell.y = startGridY;
    
 

       // now we have everythin set up
       // next, we will walk down the segment,  adding
       // all grid cells it intersects
       
        // The first cell is a special case.  Handle it before 
        // the main loop
        // we need to add its left and below cells
        
        // ALSO need to check vert or horiz seg on cell boundary!!
        //  Horizontal and vertical are special cases,  handle them seperately

        addLeft = std::fmod( r[i].x1, cellSize) == 0.0 ;
        addDown = std::fmod( r[i].y1, cellSize) == 0.0 ;

        //std::cerr<< "startGrid: " << startGridX << ", " <<startGridY << std::endl;
        //std::cerr<< "endGrid  : " << endGridX << ", " << endGridY << std::endl
        //         << "goingup  : " << goingUp << std::endl
        //         << "addLeft  : " << addLeft << std::endl
        //         << "addDown  : " << addDown << std::endl;
        //SPECIAL CASE.  Horizontal and vertical segs
        if( r[i].y1 == r[i].y2 ) {  // horizontal
            if( addLeft && addDown ){
                currCell.x--;
                currCell.y--;
            }
            else if( addLeft ){
                currCell.x--;
            }
            else if( addDown ){ // on a cell boundary, put in left cell
                currCell.y--;
            }
            for( int j = currCell.x; j <= endGridX; j++ )
            {
                cellDict[ currCell ].push_back( i );
                usedCells.insert( currCell );
                currCell.x++;
            }
            continue;
        }
        if( r[i].x1 == r[i].x2 ) {  // vertical
            if( addLeft && addDown ){
                currCell.x--;
                currCell.y--;
            }
            else if( addLeft ){
                currCell.x--;
            }
            else if( addDown ){ // on a cell boundary, put in left cell
                currCell.y--;
            }
            for( int j = currCell.y; j <= endGridY; j++ )
            {
                cellDict[ currCell ].push_back( i );
                usedCells.insert( currCell );
                // we are always going up in this case
                currCell.y++;
            }
            continue;
        }

        // handle the first cell as a special case
        // check if we need to add a diag, left, or below

        // boundary check for the diagonal cell
        if( addLeft && addDown )
        {
            currCell.x--;
            currCell.y--;
            cellDict[ currCell ].push_back( i );
            usedCells.insert( currCell );
            //std::cerr << "L&D: " << currCell.x << "," << currCell.y << std::endl; 
            currCell.x++;
            // if the seg is going down,  we want to start at the reduced Y
            // if not, we want to start on the orig Y cell
            if( goingUp )
            {
                currCell.y++;
                //std::cerr << "up" << std::endl;
            }
        }
        // if left end point is on a cell boundary, add the neighbor cell
        else {
            if( addLeft )
            {
                currCell.x--;
                cellDict[ currCell ].push_back( i );
                usedCells.insert( currCell );
                //std::cerr << "L: " << currCell.x << "," << currCell.y << std::endl; 
                currCell.x++;
            }
            // repeat the boundary check for the cell below currCell 
            else if( addDown )
            {
                currCell.y--;
                //std::cerr << "add down but no add"<<std::endl;
                // only put Y seg back if seg is going upwards
                if( goingUp ){
                    // since we record the current cell in the loop,
                    // only record the cell if we are going up
                    // otherwise,  it will get recorded twice.
                    cellDict[ currCell ].push_back( i );
                    usedCells.insert( currCell );
                    //std::cerr << "D: " << currCell.x << "," << currCell.y << std::endl; 
                    currCell.y++;
                }
            }
        } 
       
        //  if we are going down,  and the final point is on a 
        // horizontal cell boundary,  add the cell below.
        // even if it is at the diagonal,  we only need to add the cell below,
        // since we add the lower left diag cell on diags.
        if( !goingUp 
            &&  std::fmod( r[i].y2, cellSize) == 0.0){
            cell lastExtraCell;
            lastExtraCell.x = endGridX;
            lastExtraCell.y = endGridY-1;
            cellDict[ lastExtraCell ].push_back( i );
            usedCells.insert( lastExtraCell );
            //std::cerr << "last extra cell: " << lastExtraCell.x << ", "<< lastExtraCell.y << std::endl;
        }

        while( true )
        {
            // record the current cell
            cellDict[ currCell ].push_back( i );
            usedCells.insert( currCell );
            //std::cerr << "add: " << currCell.x << "," << currCell.y << std::endl;
            // check to see if we are finished (if the curr cell 
            // is equal to the end cell)
            if( currCell.x >= endGridX && 
                    ((goingUp && currCell.y >= endGridY) 
                     || ( !goingUp && currCell.y <= endGridY )
                    )
              )
                break;

            // Now we need to find which cell to visit next.  
            // find the points for uppr right and lower right corner
            // of the current cell. use colinearity test to see
            // if this segment travels up, diagonal-up, right,
            // diagnal-down,  or down.
            diagX = (currCell.x+1) * cellSize;
            diagHiY = (currCell.y+1) * cellSize;
            diagLoY = (currCell.y) * cellSize;
            colinValHi = collinearValue( r[i].x1, r[i].y1, diagX, diagHiY, r[i].x2, r[i].y2 ); 
            colinValLo = collinearValue( r[i].x1, r[i].y1, diagX, diagLoY, r[i].x2, r[i].y2 );
            if( std::abs( colinValHi ) < 0.0000001 )
            {
                // goes through upperdiagonal
                // increas currCell x and y.
                // add the diagonal cell.  
                // We only add adjacents for left and lower boundary
                currCell.x++;
                currCell.y++;
                //cellDict[ currCell ].push_back( i );
                //usedCells.insert( currCell );
                //std::cerr << "diag Hi" << std::endl;
            }
            else if( colinValHi > 0 )
            {
                // goes through upper cell boundary
                currCell.y++;
                //cellDict[ currCell ].push_back( i );
                //usedCells.insert( currCell );
                // cell is set for nect iteration
                //std::cerr << "Hi" << std::endl;
            }
            else if( std::abs(colinValLo) < 0.0000001 )
            {
                // goes through lower diagonal.  
                // add the cell below and the cell diagonal
                currCell.y--;
                cellDict[ currCell ].push_back( i );
                usedCells.insert( currCell );
              
                currCell.x++;
                //cellDict[ currCell ].push_back( i );
                //usedCells.insert( currCell );
                //std::cerr << "diag Lo" << std::endl;
            }
            else if( colinValLo > 0 )
            {
                // goes throug right cell boundary
                // add the cell to the right
                currCell.x++;
                //cellDict[ currCell ].push_back( i );
                //usedCells.insert( currCell );
                //std::cerr << "Right" << std::endl;
            } else
            {
                // it goes though the lower cell boundary
                currCell.y--;
                //cellDict[ currCell ].push_back( i );
                //usedCells.insert( currCell );
                //std::cerr << "Lo" << std::endl;
            }
       }
    }
}


/***
 *  Perform an overlay of r1 and r2.
 *  
 *  Assumes segs have interiorBelow label set appropriately.
 *
 */
void gridOverlay( const std::vector<seg2D> &r1, const seg2D r1Bbox, 
        const std::vector<seg2D> & r2,const seg2D r2Bbox, 
        std::vector<seg2D> & newR1, std::vector<seg2D> & newR2, 
        const double segsPerCell )
{
    stopwatch swTotal, swAssign, swSetIntersect, swSegIntersect, swConstruct, swTopo;
    // if the bBoxes don't intersect,  then the regions don't either.
    if (! r1Bbox.bBoxIntersect( r2Bbox ) )
        return;
   
    swTotal.start();
    // 1) we need some information. We need to 
    // get an idea of how big or small cells should be.
    // We will choose cell sizes based on the length of segments.  
    // The **segsPerCell** parameter will indicate about how many cells a seg 
    // should cross, on average, in the X or Y direction
    
    // To find average length of segs,  lets sample about 10% of segs.
    const double SEGSAMPLERATE = .1;
    int loopStep = r1.size() / (1+int((SEGSAMPLERATE * r1.size()))); 
    double sum = 0;
    int r1segCounter = 0, r2segCounter=0;
    for( int i = 0; i < r1.size(); i+= loopStep )
    {
        sum +=  r1[i].squaredLength();
        r1segCounter++;
    }

    loopStep = r2.size() / (1+int((SEGSAMPLERATE * r2.size()))); 
    for( int i = 0; i < r2.size(); i+= loopStep )
    {
        sum +=  r2[i].squaredLength();
        r2segCounter++;
    }

    double avgLen = std::sqrt( sum/(r1segCounter+r2segCounter) );
    double cellSize = avgLen/segsPerCell;
 //   cellSize = 1; 
    std::cerr << "loopStep: " << loopStep << std::endl
         << "sum:      " << sum << std::endl
         << "avgLen:   " << avgLen << std::endl
         << "cellSize: " << cellSize << std::endl;

    // declare the needed hashes and sets
    std::unordered_map< cell, std::vector<int>, cellHash, cellEqual> cellDict1;
    std::unordered_map< cell, std::vector<int>, cellHash, cellEqual> cellDict2;
    std::set< cell, cellLess > usedCells1;
    std::set< cell, cellLess > usedCells2;
    std::vector< cell > usedCells;
    swAssign.start();
    // assign the segments to cells
#pragma omp parallel sections
    {
#pragma omp section
        {
            assignCells( r1, cellSize, cellDict1, usedCells1, r2Bbox);
           // std::cerr << "assign 1 done "<< omp_get_thread_num() << " " <<std::endl;
        }
#pragma omp section
        {
            assignCells( r2, cellSize, cellDict2, usedCells2, r1Bbox);
            //std::cerr << "assign 2 done "<< omp_get_thread_num()<< " " <<std::endl;
        }
    }
    swAssign.stop();
#ifdef DEBUGPRINT    
    std::cerr<< "printing usedCells1" << std::endl;
    for (auto it = usedCells1.begin(); it != usedCells1.end(); ++it) 
    {
        std::cerr << it->x << ", " << it->y << std::endl;
    }
    std::cerr << "printing cellDict1 "<< std::endl;
    for (auto it = usedCells1.begin(); it != usedCells1.end(); ++it) 
    {
        std::cerr << it->x << ", " << it->y << ":  ";
        for( int i = 0; i < cellDict1[ *it ].size(); i++ )
            std::cerr << r1[cellDict1[ *it ][i]].x1 << ", "
                << r1[cellDict1[ *it ][i]].y1 << "   "
                << r1[cellDict1[ *it ][i]].x2 << ", "
                << r1[cellDict1[ *it ][i]].y2 << " % ";
        std::cerr << std::endl;
    }

    std::cerr<< "printing usedCells2" << std::endl;
    for (auto it = usedCells2.begin(); it != usedCells2.end(); ++it) 
    {
        std::cerr << it->x << ", " << it->y << std::endl;
    }
    std::cerr << "printing cellDict2 "<< std::endl;
    for (auto it = usedCells2.begin(); it != usedCells2.end(); ++it) 
    {
        std::cerr << it->x << ", " << it->y << ":  ";
        for( int i = 0; i < cellDict2[ *it ].size(); i++ )
            std::cerr << r2[cellDict2[ *it ][i]].x1 << ", "
                << r2[cellDict2[ *it ][i]].y1 << "   "
                << r2[cellDict2[ *it ][i]].x2 << ", "
                << r2[cellDict2[ *it ][i]].y2 << " % ";
        std::cerr << std::endl;
    }
#endif

    // We only need to examine cells that are used by both 
    // regions.  So, we need a set intersection operation
    // on the **usedCells** sets.
    swSetIntersect.start();
    for (auto it = usedCells1.begin(); it != usedCells1.end(); ++it) 
    {
        if( usedCells2.find( *it ) != usedCells2.end() )
        {
            {
                // we have a common cell,  record it
                usedCells.push_back( *it );
            }
        }
    }
    swSetIntersect.stop();
  //  std::cerr << "set intersection done"<<std::endl;

#ifdef DEBUGPRINT
    std::cerr << "printing multi-cells" << std::endl;
    for( auto it = usedCells.begin(); it != usedCells.end(); ++it )
    {
        std::cerr << it->x << ", " << it->y << std::endl;
    }
#endif

    // Now we know which cells have segs from both regions.
    // For each of those cells, test for intersections
    swSegIntersect.start();
    pAppendVector r1Points, r2Points;
    redBlueLineSegmentIntersection(r1, cellDict1, r2, cellDict2, usedCells, r1Points, r2Points );    
    swSegIntersect.stop();
  //  std::cerr << "intersecting done"<<std::endl;
    
#ifdef DEBUGPRINT
    std::cerr<< "r1 intersection points" << std::endl;
    for( int i = 0; i < r1Points.size(); i++ )
    {
        std::cerr << r1Points[i].x << "," << r1Points[i].y << " -- " << r1Points[i].index << std::endl;
    }
    std::cerr<< "r2 intersection points" << std::endl;
    for( int i = 0; i < r2Points.size(); i++ )
    {
        std::cerr << r2Points[i].x << "," << r2Points[i].y << " -- " << r2Points[i].index << std::endl;
    }
#endif

    // We now have all the intersection points,
    // we need to break the segments at those points.
    // Use the constructSegs function.
    newR1.clear();
    newR2.clear();
    swConstruct.start();
#pragma omp parallel sections
    {
#pragma omp section
        {
            constructSegs( r1, r1Points, newR1 );
           // std::cerr << "construct 1 done"<<std::endl;
        }
#pragma omp section
        {
            constructSegs( r2, r2Points, newR2 );
           // std::cerr << "construct 2 done"<<std::endl;
        }
    }
    swConstruct.stop();
#ifdef DEBUGPRINT
    std::cerr << "======================== " << std::endl;

    std::cerr << "new r1: " << std::endl;
    for( int i = 0; i < newR1.size(); i++ )
    {
        newR1[i].print();
    }
    std::cerr << "new r2: " << std::endl;
    for( int i = 0; i < newR2.size(); i++ )
    {
        newR2[i].print();
    }
#endif

    // We now have the segmewnts broken up appropriately.
    // The final step is to construct the topology.
    // There is a question as to whether we should do this on the fly 
    // during the intersection point calculation.
    
    // also need the cell size values.
    //std::cerr << "num threads " << omp_get_num_threads() << std::endl;
    swTopo.start();
//#pragma omp parallel sections
    {
//#pragma omp section
        {
           // std::cerr << "threadnum " << omp_get_thread_num() << std::endl;
            computeTopology( newR1, r2, usedCells2, cellDict2, cellSize ); 
          //  std::cerr << "===========topo 1 done " << std::endl;
        }
//#pragma omp section
        {
            //std::cerr << "threadnum " << omp_get_thread_num() << std::endl;
            computeTopology( newR2, r1, usedCells1, cellDict1, cellSize ); 
           // std::cerr << "===========topo 2 done " << std::endl;
        }

    }
    swTopo.stop();
    swTotal.stop();
#ifdef DEBUGPRINT
    std::cerr << "new r1: " << std::endl;
    for( int i = 0; i < newR1.size(); i++ )
    {
        newR1[i].print();
    }
    std::cerr << "new r2: " << std::endl;
    for( int i = 0; i < newR2.size(); i++ )
    {
        newR2[i].print();
    }
#endif
    std::cerr << "Total time    : " << swTotal.getTime() <<std::endl;
    std::cerr << "Assign time   : " << swAssign.getTime() <<std::endl;
    std::cerr << "Set Inter time: " << swSetIntersect.getTime() <<std::endl;
    std::cerr << "Seg Inter time: " << swSegIntersect.getTime() <<std::endl;
    std::cerr << "Construct time: " << swConstruct.getTime() <<std::endl;
    std::cerr << "Topo time     : " << swTopo.getTime() <<std::endl;
}

/**
 * To compute topolgy,  we need to take the left end point of a segment,  compute its cell, 
 * then find the closeset cell from the opposing region below that cell so we can see the 
 * interiorAbove value of the nearest segment.
 *
 * In essence, this is a ray shooting algorithm, and uses the traditional tricks for ray shooting
 * (ignore verticals,  only count a ray through a left end point of a segment(and not through a right end point)).
 * 
 * the cellDict is sorted by Y then by X, perfect for point in poly test!
 */ 
void computeTopology( 
        std::vector<seg2D> &newR1,
        const std::vector<seg2D> &r2, 
        const std::set< cell, cellLess > & usedCells2,
        std::unordered_map< cell, std::vector<int>, cellHash, cellEqual> & cellDict2,
        const int cellSize )
{
    // get a seg from newR1,  find the nearest cell below that segs left end point in cellDict2,  test
    // the segs in that cell to find the nearest one to the seg from r1, and get its interiorAbove value.
#pragma omp parallel for schedule(dynamic, 500) 
    for( int i = 0; i < newR1.size(); i++ )
    {
        double closestDistance = std::numeric_limits<double>::max();
        double closestSlope = -1 * std::numeric_limits<double>::max();
        double theSlope, theDistance;
        bool collinearSeg = false;
        bool vertical = false;
        seg2D r2Seg;
        double checkPointX, checkPointY;
        // get the cell of the seg's left end point.
        cell currentCell;
        currentCell.x = int(newR1[i].x1 / cellSize );
        currentCell.y = int(newR1[i].y1 / cellSize );
        
        // adjust current cell if on a boundary
        bool addLeft = std::fmod( newR1[i].x1, cellSize) == 0.0 ;
        bool addDown = std::fmod( newR1[i].y1, cellSize) == 0.0 ;
        if( addLeft && addDown ){
            currentCell.x--;
            currentCell.y--;
        }
        else if( addLeft ){
            currentCell.x--;
        }
        else if( addDown ){ 
            currentCell.y--;
        }
        //std::cerr << "NEW SEG: ";
        //newR1[i].print();
        //std::cerr <<"cell: " << currentCell.x << "," << currentCell.y<<std::endl;


        // find the nearest cell from region2
        auto it = usedCells2.lower_bound( currentCell );
        
        // if currentCell has a higher Y value than any cell in
        // usedCells2 in the same X colume,  then **it** will
        // point to a cell with the 
        // next highest X value (the next column),  or end().  
        // We need to back the iterator up 1 spot to find the appropriate 
        // cell to compare against.
        if( (it == usedCells2.end()) 
                || (it != usedCells2.begin() && it->x > currentCell.x)
                || (it != usedCells2.begin() && it->x == currentCell.x && it->y > currentCell.y ))
            it--;
        //std::cerr << "other cell: " << it->x << "," << it->y << std::endl;
        if( (it->x == currentCell.x && it->y > currentCell.y)
                || it->x != currentCell.x)
            continue;

        // it is possible that do we not acutally find a seg in the cell that spans
        // the left end point of newR1[i]. In that case,  we will have to proceed
        // down the grid.
        while( 
                ! collinearSeg 
                && closestDistance == std::numeric_limits<double>::max()
                && it->x == currentCell.x )
        {
            // now,  we go through that cell and find the
            // closest seg to the left end point
            //  of newR1[i] that is below the left 
            //  end point of newR1[i], and then get its 
            //  interiorAbove value
            //std::cerr << "loop cell: " << it->x << "," << it->y << std::endl;
            for( int j = 0; j < cellDict2[*it].size(); j++ )
            {
                r2Seg = r2[cellDict2[*it][j]];

                //r2Seg.print();

                // skip verticals unless collinear and overlapping
                if( r2Seg.x1 == r2Seg.x2  )
                {
                    vertical = true;
                    if(  std::abs( collinearValue( 
                                    newR1[i].x1, newR1[i].y1, 
                                    newR1[i].x2, newR1[i].y2,
                                    r2Seg.x1, r2Seg.y1 ) ) < 0.0000001 
                            && ( (r2Seg.y1 < newR1[i].y1 && newR1[i].y1 < r2Seg.y2 ) 
                                || (r2Seg.y1 < newR1[i].y2 && newR1[i].y2 < r2Seg.y2 )
                                || (newR1[i].y1 < r2Seg.y1  &&  r2Seg.y1 < newR1[i].y2 )
                                || (newR1[i].y1 < r2Seg.y2  &&  r2Seg.y2 < newR1[i].y2 ) 
                                || (newR1[i].x1 == r2Seg.x1 && newR1[i].y1 == r2Seg.y1 
                                    && newR1[i].x2 == r2Seg.x2 && newR1[i].y2 == r2Seg.y2 ) ) )
                    {
                        // vertical, collinear, and overlapping
                        collinearSeg = true;
                        newR1[i].overlapInteriorAbove = r2Seg.interiorAbove;
                        newR1[i].overlapInteriorBelow = r2Seg.interiorBelow;
                        //std::cerr << "vert colin overlap" <<std::endl;
                        break;
                    }
                    else
                    {
                        // just vertical
                        continue;
                    }
                }
                else
                {
                    // the r2 seg is NOT vertical.
                    // check if r2Seg spans the point
                    if( r2Seg.x1 <= newR1[i].x1 && newR1[i].x1 < r2Seg.x2 )
                    {
                        //std::cerr << "span is good" <<std::endl;
                        // the span is good.
                        // check if they are collinear (and overlapping )
                        if( std::abs( collinearValue (
                                        newR1[i].x1, newR1[i].y1, 
                                        newR1[i].x2, newR1[i].y2,
                                        r2Seg.x1, r2Seg.y1 ) ) < 0.0000001 
                                && std::abs( collinearValue (
                                        newR1[i].x1, newR1[i].y1, 
                                        newR1[i].x2, newR1[i].y2,
                                        r2Seg.x2, r2Seg.y2 ) ) < 0.0000001 )
                        {
                            collinearSeg = true;
                            newR1[i].overlapInteriorAbove = r2Seg.interiorAbove;
                            newR1[i].overlapInteriorBelow = r2Seg.interiorBelow;
                            //std::cerr << "colin and overlapping" << std::endl;
                            break;
                        }
                        // if we get here,  they are not collinear
                        // check if the end point is on r2Seg
                        if( (newR1[i].x1 == r2Seg.x1 &&  newR1[i].y1 == r2Seg.y1) 
                                || (std::abs( collinearValue (
                                    r2Seg.x1, r2Seg.y1,
                                    r2Seg.x2, r2Seg.y2,
                                    newR1[i].x1, newR1[i].y1  ) ) < 0.0000001 ) )
                        {   
                            //std::cerr << "r1 point on other seg " << std::endl;
                            // the left end point of the r1 seg is ON the r2 seg
                            // if r2Seg is below the r1Seg,  we need to see if
                            // it is nearest the r1Seg (its slope will need to be
                            // closest to 0
                            // use left hadnd turn test for below testing
                            theSlope = (r2Seg.y2-r2Seg.y1) / (r2Seg.x2-r2Seg.x1);
                            if( collinearValue (
                                            newR1[i].x1, newR1[i].y1,
                                            newR1[i].x2, newR1[i].y2,
                                            r2Seg.x2, r2Seg.y2 )  < 0  
                                    && ( closestDistance > 0
                                        || (closestDistance == 0 && theSlope> closestSlope) ) )
                            {
                                //std::cerr << "found closest a" <<std::endl;
                                closestDistance = 0.0;
                                closestSlope = theSlope;
                                newR1[i].overlapInteriorAbove = r2Seg.interiorAbove;
                                newR1[i].overlapInteriorBelow = r2Seg.interiorAbove;
                            }
                        }
                        else if( closestDistance > 0 )
                        {
                            // r1Seg end point is NOT on the r2Seg,
                            // so we need to see if r2Seg is below r1Seg,
                            // and then if it is the closest we have yet seen.
                            // basically,  get the y value of the r2Seg at the 
                            // x value of the left end point of the r1Seg.
                            double x = newR1[i].x1;
                            double s2y, theDistance;
                            //std::cerr << "r1 point NOT on r2 seg " <<std::endl;

                            if( newR1[i].x1 == r2Seg.x1 )
                            {
                                s2y = r2Seg.y1;
                            }
                            else
                            {
                                s2y = ((r2Seg.y2*x - r2Seg.y2*r2Seg.x1-r2Seg.y1*x 
                                            + r2Seg.y1*r2Seg.x1) 
                                        / (r2Seg.x2-r2Seg.x1))+r2Seg.y1;
                            }
                            theDistance = newR1[i].y1 - s2y;
                            theSlope = (r2Seg.y2-r2Seg.y1) / (r2Seg.x2-r2Seg.x1); 
                            
                            if( theDistance > 0 && 
                                    (theDistance < closestDistance
                                     || (theDistance == closestDistance 
                                         && theSlope > closestSlope) ) )
                            {
                                //std::cerr << "found closest b"<<std::endl;
                                closestDistance = theDistance;
                                closestSlope = theSlope;
                                newR1[i].overlapInteriorAbove = r2Seg.interiorAbove;
                                newR1[i].overlapInteriorBelow = r2Seg.interiorAbove;
                            }
                        }
                    }
                }
            }
            // if we get here,  we have done through everything in this cell,
            // so increase the iterator to get the next cell.
	        if(it == usedCells2.begin()){
                break;
            }
            it--;
        }
    }
}

/** 
 * Break the segments at the points indicated in **pois**
 *
 *  **indexedPoint** type has an index.  The index refers to the segmen in **r1**.
 *  The corresponding segment will be broken at the point indicated by the **indexPoint** instance.
 */
void constructSegs( 
        const std::vector<seg2D> & r1,
        pAppendVector & pois, 
        std::vector<seg2D> & newR1 )
{
    seg2D currSeg;
    unsigned int index = 0;
    // sort the pois
    std::sort( pois.begin(), pois.end(), indexedPointLess );
    // traverse the segments in r1.  If the segment does not
    // need to be broken, copy it to newR1
    // if the segment needs to be broken at intersection points,
    // construct the new segs and add them to newR1
    for( int i =0; i < r1.size(); i++ )
    {
        currSeg = r1[i];
        // if the intersection point's index is equal to the 
        // current segment's index,  break up the segment.
        while( index < pois.size() && pois[index].index == i )
        {
            // sometimes an intersection point is reported multiple times
            // the following loop skips over repeated points.
            while( index < pois.size()-1 && pois[index] == pois[index+1] )
                index++;
            currSeg.x2 = pois[index].x;
            currSeg.y2 = pois[index].y;
            newR1.push_back( currSeg );
            currSeg.x1 = currSeg.x2;
            currSeg.y1 = currSeg.y2;
            index ++;
        }
        currSeg.x2 = r1[i].x2;
        currSeg.y2 = r1[i].y2;
        newR1.push_back( currSeg );
    }

    // We may need to remove duplicate segments
    // and degenerate segments at this point.  
}


/**
 *  Find intersection points between lines in the red set and lines in the blue set
 *
 *  Line segments are assigned to cells.  Lines can only intersect if they are in the same cell.
 */
void redBlueLineSegmentIntersection(
        const std::vector<seg2D>& r1, std::unordered_map< cell, std::vector<int>, cellHash, cellEqual> & cellDict1, 
        const std::vector<seg2D> &r2, std::unordered_map< cell, std::vector<int>, cellHash, cellEqual> & cellDict2,
        const std::vector< cell > & usedCells,
        pAppendVector & r1Points, pAppendVector & r2Points )    
{
#pragma omp parallel for schedule( dynamic, 100 )
    for( int k = 0; k < usedCells.size(); k++ )
    {
        double x1,y1,x2,y2,x3,y3,x4,y4; // used to refer to segment end points
        double ll, lu, rl, ru;  // used for collinear & overlapping.  
        // LeftLower, LeftUpper, RightLower, RightUpper
        unsigned int r1Index, r2Index;
        double denom, ua, ub, x, y;
        for( int i = 0; i < cellDict1[ usedCells[k] ].size(); i++ )
        {   
            r1Index = cellDict1[ usedCells[k] ][i];
            x1 = r1[r1Index].x1;
            y1 = r1[r1Index].y1;
            x2 = r1[r1Index].x2;
            y2 = r1[r1Index].y2;
            for( int j = 0; j < cellDict2[ usedCells[k] ].size(); j++ )
            {
                r2Index = cellDict2[ usedCells[k] ][j];
                x3 = r2[r2Index].x1;
                y3 = r2[r2Index].y1;
                x4 = r2[r2Index].x2;
                y4 = r2[r2Index].y2;

                // check if the segs are collinear and overlapping
                // use the left hand turn test
                if( std::abs( collinearValue( x1, y1, x2, y2, x3, y3 ) ) < 0.0000001
                    && std::abs( collinearValue( x1, y1, x2, y2, x4, y4 ) ) < 0.0000001 )
                {
                    // if the segs are vertical,  we need to use the Y vals for comparison
                    ll = x1;
                    lu = x2;
                    rl = x3;
                    ru = x4;
                    if( std::abs( x1-x2 ) < 0.0000001 )
                    {
                        ll = y1;
                        lu = y2;
                        rl = y3;
                        ru = y4;
                    }
                    if( rl < ll && ll < ru ) {
//#pragma omp critical (ompCriticalR2)
                        r2Points.append( indexedPoint( r2Index, x1, y1 ) );
                    }
                    if( rl < lu && lu < ru ){
//#pragma omp critical (ompCriticalR2)
                        r2Points.append( indexedPoint( r2Index, x2, y2 ) );
                    }
                    if( ll < rl && rl < lu ){
//#pragma omp critical (ompCriticalR1)
                        r1Points.append( indexedPoint( r1Index, x3, y3 ) );
                    }
                    if( ll < ru && ru < lu ){
//#pragma omp critical (ompCriticalR1)
                        r1Points.append( indexedPoint( r1Index, x4, y4 ) );
                    }
                    continue;
                }

                // if an endpoint is shared,  we do not count the intersection
                if( (x1 == x3 && y1 == y3 ) 
                        || ( x1 == x4 && y1 == y4 )
                        || ( x2 == x3 && y2 == y3 )
                        || ( x2 == x4 && y2 == y4 ) )
                {
                    continue;
                }
                // if denom is 0, lines are parallel,  no intersection
                denom = ((y4-y3)*(x2-x1)) - ((x4-x3)*(y2-y1));
                if( denom == 0.0 )
                    continue;

                // if we get here,  lines are not parallel,  they must
                // intersect somewhere.  We need to determine
                // if the intersection is within the line segment
                ua = ((x4-x3)*(y1-y3))-((y4-y3)*(x1-x3));
                ua = ua / denom;
                ub = ((x2-x1)*(y1-y3))-((y2-y1)*(x1-x3));
                ub = ub / denom;

                if( 0.0 < ua && ua < 1.0 && 0.0 <= ub && ub <= 1.0 )
                {
                    // intersection in interior of seg from r1
                    x = x1 + ( ua * (x2-x1) );
                    y = y1 + ( ua * (y2-y1) );
                    if( ! ( (x == x1 && y == y1) || (x == x2 && y == y2 ) ) )
                    {
//#pragma omp critical (ompCriticalR1)
                        r1Points.append( indexedPoint( r1Index, x, y ) );
                    }
                }
                if( 0.0 <= ua && ua <= 1.0 && 0.0 < ub && ub < 1.0 )
                {
                    // intersection in interior of seg from r1
                    x = x1 + ( ua * (x2-x1) );
                    y = y1 + ( ua * (y2-y1) );
                    if( ! ( (x == x3 && y == y3) || (x == x4 && y == y4 ) ) )
                    {
//#pragma omp critical (ompCriticalR2)
                        r2Points.append( indexedPoint( r2Index, x, y ) ); 
                    }
                }
            }
        }
    }
    
    // we now have all the intersection points,  return them
    // we will still need to break the input segments at those points.
}


