/*
 * The MIT License (MIT)
 * Copyright (c) <2017> <Mark McKenney, Yanan Da>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include "d2hex.h"
#include "gridOverlay.h"
#include <fstream>
#include <omp.h>

/** \mainpage
 *
 * This documentation describes code implementing regular grid based line segment intersection algorithms for computing the overlay of regions and shared memory parallelization techniques for the algorithms. The implementation uses OpenMP for parallelization.

 *This code includes two versions of algorithms. One is hash table based algorithm using hash tables to store the grid information. The second implementation is sorted vector based which uses vectors to store the grid information.

 *The code computes the overlay of two input regions. The line segments that are returned are labeled such that the intersection, union, or difference can be computed by keeping line segments with the proper labels.

 *Source code for the project is available at:

 *https://bitbucket.org/marmcke/code_spatialdecompositionoverlay
 *
 */

using namespace std;

/**
 * Function that tokenizes a string.  
 * @param [in] str: the string to tokenize
 * @param [out] tokens: a vecotr of string tokens
 * @param [in] delimiters:  A string consisting of delimiters on which to tokenize
 */
void tokenizeString(const std::string& str, 
        std::vector<string>& tokens, 
        const string& delimiters );

/**
 * The main function provides examples of how to call the parallel overlay function 
 *
 * This particular function expects input in a hexadecimal format, and includes code
 * to extract floating point numbers from exadecimal representation.
 *
 * The command line arguments required are:
 *  - [an input hex file with region 1]
 *  - [an input hex file with region 2]
 *  - [segsPerCell]
 *  - [number of threads to use]
 *  
 */
int main( int argc, char * argv[] ) 
{
    // read the argument regions
    std::string inputFileName1, inputFileName2;
    vector< seg2D >v1, v2, newR1, newR2;
    seg2D v1Bbox, v2Bbox;
    double segsPerCell = 1;
    int numberOfThreads = omp_get_num_procs();

    if( argc < 3 )
    {
        std::cerr << "usage: exe  [input file name 1] [input file name 2] [segsPerCell] [number of threads to use]" << std::endl;
        exit( -1 );
    }
    {
        std::stringstream ss1;
        ss1 << argv[1];
        ss1 >> inputFileName1;
    }
    {
        std::stringstream ss1;
        ss1 << argv[2];
        ss1 >> inputFileName2;
    }
    if( argc >= 4)
    {
        std::stringstream ss1;
        ss1 << argv[3];
        ss1 >> segsPerCell;
    }
    if( argc >= 5)
    {
        std::stringstream ss1;
        ss1 << argv[4];
        ss1 >> numberOfThreads;
    }

    ifstream inFileStrm1;
    inFileStrm1.open( argv[1] );
    if( ! inFileStrm1 )
    {
        cerr << "Error: could not open file: " << argv[1] << endl;
        exit( -1 );
    }
    ifstream inFileStrm2;
    inFileStrm2.open( argv[2] );
    if( ! inFileStrm2 )
    {
        cerr << "Error: could not open file: " << argv[2] << endl;
        exit( -1 );
    }

    cerr << "Reading files: " << argv[1] << ", " <<argv[2] << endl;
    vector<string> splitLine;
    string line("  ");
    double x, y;
    int la, lb;
    int i = 0;
    while(inFileStrm1.good() )
    {
        getline(inFileStrm1, line);
        if( inFileStrm1.good() )
        {
            if( line.size() == 0 || line[0] == '#' )
                continue;
            splitLine.clear();
            string delim(" \t" );
            tokenizeString( line, splitLine, delim );
            v1.push_back( seg2D() );
            v1[i].x1 = doubleHexConverter::hex2d(splitLine[0]);
            v1[i].y1 = doubleHexConverter::hex2d(splitLine[1]);
            v1[i].x2 = doubleHexConverter::hex2d(splitLine[2]);
            v1[i].y2 = doubleHexConverter::hex2d(splitLine[3]);
            stringstream ss1( splitLine[4] );
            ss1 >> la;
            stringstream ss2( splitLine[5] );
            ss2 >> lb;
            v1[i].interiorAbove = la;
            v1[i].interiorBelow = lb;
            v1[i].orderEndPoints();
            v1Bbox.growBbox( v1[i] );
            i++;
        }
    }
    cerr <<"file 1 finished reading"<<endl;
    i = 0;
    while(inFileStrm2.good() )
    {
        getline(inFileStrm2, line);
        if( inFileStrm2.good() )
        {
            if( line.size() == 0 || line[0] == '#' )
                continue;
            splitLine.clear();
            string delim(" \t" );
            tokenizeString( line, splitLine, delim );
            v2.push_back( seg2D() );
            v2[i].x1 = doubleHexConverter::hex2d(splitLine[0]);
            v2[i].y1 = doubleHexConverter::hex2d(splitLine[1]);
            v2[i].x2 = doubleHexConverter::hex2d(splitLine[2]);
            v2[i].y2 = doubleHexConverter::hex2d(splitLine[3]);
            stringstream ss1( splitLine[4] );
            ss1 >> la;
            stringstream ss2( splitLine[5] );
            ss2 >> lb;
            v2[i].interiorAbove = la;
            v2[i].interiorBelow = lb;
            v2[i].orderEndPoints();
            v2Bbox.growBbox( v2[i] );
            i++;
        }
    }
    cerr <<"file 2 finished reading"<<endl;
    
    omp_set_num_threads ( numberOfThreads );
    std::cerr << "omp max threads: " << omp_get_max_threads() << std::endl;
    std::cerr << "omp num procs: " << omp_get_num_procs() << std::endl;
    
    gridOverlay( v1, v1Bbox, v2, v2Bbox, newR1, newR2, segsPerCell ); 
    
    std::cerr <<" input segs, output segs for r1: " << v1.size() << " -- " << newR1.size() << std::endl;
    std::cerr <<" input segs, output segs for r2: " << v2.size() << " -- " << newR2.size() << std::endl;

    // final bit is to write to a file for plotting,  just to check that everything works.
    
    if( false )
    {
        std::cerr << "printing"<<std::endl;
        ofstream outfile( "zzzOverlay.hex", std::ios_base::out | std::ios_base::trunc );
        for( int i = 0; i < newR1.size(); i++ )
        {
            outfile << doubleHexConverter::d2hex( newR1[i].x1 ) << " " 
                    << doubleHexConverter::d2hex( newR1[i].y1 ) << " "
                    << doubleHexConverter::d2hex( newR1[i].x2 ) << " "
                    << doubleHexConverter::d2hex( newR1[i].y2 ) << " "
                    << newR1[i].interiorAbove << " "
                    << newR1[i].interiorBelow << std::endl;
        }
        for( int i = 0; i < newR2.size(); i++ )
        {
            outfile << doubleHexConverter::d2hex( newR2[i].x1 ) << " " 
                    << doubleHexConverter::d2hex( newR2[i].y1 ) << " "
                    << doubleHexConverter::d2hex( newR2[i].x2 ) << " "
                    << doubleHexConverter::d2hex( newR2[i].y2 ) << " "
                    << newR2[i].interiorAbove << " "
                    << newR2[i].interiorBelow << std::endl;
        }
     
        outfile.close();
    }
}


void tokenizeString(const std::string& str, std::vector<string>& tokens, const string& delimiters )
{
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);	// Skip delimiters at beginning.
    std::string::size_type pos     = str.find_first_of(delimiters, lastPos);	// Find first "non-delimiter".

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
        tokens.push_back(str.substr(lastPos, pos - lastPos));	        // Found a token, add it to the vector.
        lastPos = str.find_first_not_of(delimiters, pos);		// Skip delimiters.  Note the "not_of"
        pos = str.find_first_of(delimiters, lastPos);			// Find next "non-delimiter"
    }
}

