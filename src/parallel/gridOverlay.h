/*
 * The MIT License (MIT)
 * Copyright (c) <2017> <Mark McKenney, Yanan Da>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */

#include <limits>
#include <vector>
#include <iostream> 

#ifndef GRIDOVERLAY_H
#define GRIDOVERLAY_H 

struct indexedPoint {
    unsigned int index;
    double x,y;
    indexedPoint(): index(0), x(0.0), y(0.0)
    {   }
    indexedPoint( int index, double x, double y): index( index ), x( x ), y( y )
    {   }
    bool operator == ( const indexedPoint & rhs ) const
    {
        return index == rhs.index && x == rhs.x && y == rhs.y;
    }
};

struct  {
    bool operator () ( 
            const indexedPoint & lhs,
            const indexedPoint & rhs ) const
    {
        return lhs.index < rhs.index 
                || (lhs.index == rhs.index && lhs.x < rhs.x )
                || (lhs.index == rhs.index && lhs.x == rhs.x && lhs.y < rhs.y) ; 
    }
}indexedPointLess;


struct seg2D
{
    double x1,y1,x2,y2;
    int interiorAbove, interiorBelow;
    int overlapInteriorAbove, overlapInteriorBelow;
    seg2D( ): x1(std::numeric_limits<double>::max()),
    y1(std::numeric_limits<double>::max()),
    x2(-std::numeric_limits<double>::max()),
    y2(-std::numeric_limits<double>::max()),
    interiorAbove( 0 ), interiorBelow( 0 ),
    overlapInteriorAbove( 0 ),
    overlapInteriorBelow( 0 )
    {   }

    seg2D( double x1, double y1, double x2, double y2,
            int interiorAbove, int interiorBelow, 
            int overlapInteriorAbove, int overlapInteriorBelow ): 
        x1(x1), y1(y1), x2(x2), y2(y2),
        interiorAbove( interiorAbove ), interiorBelow( interiorBelow ),
        overlapInteriorAbove( overlapInteriorAbove ),
        overlapInteriorBelow( overlapInteriorBelow )
    {   
        if( x2 < x1 
            || (x2 == x1 && y2 < y1 ) )
        {
            double tmp = x1;
            x1 = x2;
            x2 = tmp;
            tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
    }
    double inline squaredLength() const
    {
        return ((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1));
    }
    void print( bool newline=true ) const
    {
        std::cerr << "(" << x1 << ","<< y1<<")("<<x2<<","<<y2<<") "
            << interiorAbove << ":"<< interiorBelow 
            << " - " << overlapInteriorAbove << ":" << overlapInteriorBelow;
        if( newline ) 
            std::cerr << std::endl;
    }
    void orderEndPoints()
    {
        if( x2 < x1 
            || (x2 == x1 && y2 < y1 ) )
        {
            double tmp = x1;
            x1 = x2;
            x2 = tmp;
            tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
    }
    /**
     *  if we are using this seg to store a bounding box,  this function will
     *  grow the bounding box based on the segment passed to it
     *
     *  for bbox,  x1,y1 is lower left, x2 y2 is upper right
     *  
     */
    void growBbox( const seg2D s )
    {
        if( s.x1 <= x1 ) x1 = s.x1;
        if( s.x2 <= x1 ) x1 = s.x2;
        if( s.x1 >= x2 ) x2 = s.x1;
        if( s.x2 >= x2 ) x2 = s.x2;

        if( s.y1 <= y1 ) y1 = s.y1;
        if( s.y2 <= y1 ) y1 = s.y2;
        if( s.y1 >= y2 ) y2 = s.y1;
        if( s.y2 >= y2 ) y2 = s.y2;
    }

    /**
     * test if two bounding boxes intersect.  Again, we are using segs to 
     * store bouding boxes.
     *
     * for bbox,  x1,y1 is lower left, x2 y2 is upper right 
     */
    bool bBoxIntersect( const seg2D s ) const
    {
	    if(s.x1 > x2 || s.x2 < x1 || s.y1 > y2 || s.y2 < y1)
            return false;
        return true;
    }
};

void gridOverlay( 
        const std::vector<seg2D> &r1, const seg2D r1Bbox, 
        const std::vector<seg2D> & r2, const seg2D r2Bbox, 
        std::vector<seg2D> & newR1, std::vector<seg2D> & newR2, 
        const double segsPerCell = 3 );

#endif

