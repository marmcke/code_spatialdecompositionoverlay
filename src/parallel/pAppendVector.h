/*
 * The MIT License (MIT)
 * Copyright (c) <2017> <Mark McKenney>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */

#include <vector>
#include "gridOverlay.h"
#include <omp.h>
#include <iostream>
#ifndef PAPPENDVECTOR_H
#define PAPPENDVECTOR_H


class pAppendVector
{
    private:
        std::vector<indexedPoint> v;
        unsigned int elements;
        omp_lock_t appendLock;

    public:
        pAppendVector(): elements( 0 )
        { 
            omp_init_lock(&appendLock);
            v.resize( 1024 );
        }

        pAppendVector & operator = (const pAppendVector & rhs ); // no assignment allowed
    
        std::vector<indexedPoint>::iterator begin( )
        {
            return v.begin();
        }

        std::vector<indexedPoint>::iterator end( )
        {
            return v.begin() + (elements);
        }

        indexedPoint & operator [] ( unsigned int i ) 
        {
            return v[i];
        }

        void append( const indexedPoint & newPoi )
        {
            // atomically update the number of elements,
            // this reserves a spot where we can write the value.
            // next check if we need a resize.  if so,  
            // the critical section the resize
            unsigned int value;
            #pragma omp atomic capture
            value = elements++;
            // #pragma omp critical
            //std::cerr << "value " << value << "  size " << v.size() << std::endl;
            if( value >= v.size() )
            {
                // make sure no other thread has updated the size
                omp_set_lock(&appendLock);
                if( value >= v.size() )
                {
                    v.resize( v.size() * 2 );
                }
                omp_unset_lock(&appendLock);
            }
            v[value] = newPoi;
        }

        unsigned int size() const
        {
            return elements;
        }


};

#endif


