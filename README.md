
This repository contains a dynamic regular grid spatial decomposition schema for computing the overlay of regions:

Decompose the regions based on a fixed grid. The decomposition is generated on the fly, so parameters may be passed to the algorithm.
The source code includes two algorithms: a hash table based algorithm and a sorted vector based algorithm, differing in the data structure used to store the grid information.
A parallelization technique for shared memory parallelization is implemented for each algorithm.








