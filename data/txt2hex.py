import sys
import struct


#read the file on line at a time
#output the graph format to the output file
coords = []
if len( sys.argv ) < 3:
	print 'usage: exe inputTextFile outputBinFile'
if len( sys.argv ) >= 3:
	infileName = sys.argv[1]
	infile = open(infileName, 'r' )
	outfileName = sys.argv[2]
	outfile = open(outfileName, 'w' )
	for line in infile:
		if len( line ) == 0 or line[0] == '#':
			continue
		fileContents = line.split()
                if len( fileContents ) == 0:
                    continue
		x1 = float( fileContents[0] )
		y1 = float( fileContents[1] )
		x2 = float( fileContents[2] )
		y2 = float( fileContents[3] )
                if len(fileContents) >4 :
                    la = int( fileContents[4] )
		    lb = int( fileContents[5] )
		
		#convert x and y to hexx and hexy
		s=struct.pack('>d', x1)
		hexx1 = ''.join('%.2x' % ord(c) for c in s) # get hex vals from bin string s
		s=struct.pack('>d', y1)
		hexy1 = ''.join('%.2x' % ord(c) for c in s) # get hex vals from bin string s
		s=struct.pack('>d', x2)
		hexx2 = ''.join('%.2x' % ord(c) for c in s) # get hex vals from bin string s
		s=struct.pack('>d', y2)
		hexy2 = ''.join('%.2x' % ord(c) for c in s) # get hex vals from bin string s
		#output the line to the new file
		outfile.write( hexx1 + ' ' + hexy1 + ' ' + hexx2 + ' ' + hexy2)
                if len(fileContents) >4 :
                    outfile.write( ' '+  str( la ) + ' ' + str( lb ) )
                outfile.write(  '\n')




#dec 2 hex
#x = -235.23
#s=struct.pack('>d', x)
#r = ''.join('%.2x' % ord(c) for c in s) $ get hex vals from bin string s
#print r

#hex 2 dec
#print struct.unpack('>d','\xc0\x6d\x67\x5c\x28\xf5\xc2\x8f' ) #unpack a hex string
